﻿using InstagramApiSharp.API;
using InstagramApiSharp.API.Builder;
using InstagramApiSharp.Classes;
using InstagramApiSharp.Logger;
using InstagramApiTests.Utils;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramApiTests
{
    public class InstaConnector
    {
        private static IInstaApi instaApi;

        static void Main(string[] args)
        {
            var result = Task.Run(MainAsync).GetAwaiter().GetResult();
            if (result)
                return;
            Console.ReadKey();
        }

        public static async Task<bool> MainAsync()
        {
            try
            {
                Console.WriteLine("Starting IWA");
                Console.WriteLine("Type your username:");
                var userName = Console.ReadLine();

                Console.WriteLine("Type your password:");
                var pass = Console.ReadLine();

                Console.WriteLine("--- DISABLE 2FA BEFORE RUNNING ---");

                var userSession = new UserSessionData
                {
                    UserName = userName,
                    Password = pass
                };

                var delay = RequestDelay.FromSeconds(2, 2);

                instaApi = InstaApiBuilder.CreateBuilder()
                    .SetUser(userSession)
                    .UseLogger(new DebugLogger(LogLevel.All))
                    .SetRequestDelay(delay)
                    .Build();
                
                if (!instaApi.IsUserAuthenticated)
                {
                    Console.WriteLine($"Logging in as {userSession.UserName}");
                    delay.Disable();
                    var logInResult = await instaApi.LoginAsync();
                    delay.Enable();

                    await FollowerCheck.GetUnfollowCandidates(instaApi);
                    
                    Console.WriteLine("Done");
                    if (!logInResult.Succeeded)
                    {
                        Console.WriteLine($"Unable to login: {logInResult.Info.Message}");
                        return false;
                    }
                }
                var state = instaApi.GetStateDataAsStream();

         
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                //var logoutResult = Task.Run(() => instaApi.LogoutAsync()).GetAwaiter().GetResult();
                //if (logoutResult.Succeeded) Console.WriteLine("Logout succeed");
            }
            return false;
        }
    }
}
