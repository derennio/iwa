﻿using InstagramApiSharp;
using InstagramApiSharp.API;
using InstagramApiSharp.Classes.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace InstagramApiTests.Utils
{
    public class FollowerCheck
    {
        public static async Task GetRecentActivityFeed(IInstaApi api)
        {
            var recentActivity = await api.UserProcessor
    .GetRecentActivityFeedAsync(PaginationParameters.MaxPagesToLoad(1));

            List<InstaRecentActivityFeed> activityFeed = recentActivity.Value.Items;

            foreach (InstaRecentActivityFeed item in activityFeed)
            {
                Console.WriteLine($"{item.RichText}");
            }
        }

        public static async Task GetUnfollowCandidates(IInstaApi api)
        {
            var currentUser = await api.GetCurrentUserAsync();

            var followers = await api.UserProcessor.GetUserFollowersAsync(currentUser.Value.UserName,
                 PaginationParameters.MaxPagesToLoad(5));

            var following = await api.UserProcessor.GetUserFollowingAsync
                (currentUser.Value.UserName, PaginationParameters.MaxPagesToLoad(5));

            List<string> uniqueNames = new List<string>();
            List<string> verified = new List<string>();

            string dataString = "Username,isPrivate,isVerified,isFavorite,isBestie2," +
                "isFavoriteForStories,isFavoriteForHighlights,isInterest,isNeedy,hasHighlightReels\n";

            foreach (InstaUserShort name in following.Value)
            {
                if (name.IsVerified)
                    verified.Add(name.UserName);
                else
                    uniqueNames.Add(name.UserName);

                var fullData = await api.UserProcessor.GetFullUserInfoAsync(name.Pk);
                var fullInfo = fullData.Value.UserDetail;

                dataString += $"{name.UserName},{name.IsPrivate},{name.IsVerified}," +
                $"{fullInfo.IsFavorite},{fullInfo.FriendshipStatus.IsBestie}," +
                $"{fullInfo.IsFavoriteForStories},{fullInfo.IsFavoriteForHighlights},{fullInfo.IsInterestAccount},{fullInfo.IsNeedy},{fullInfo.HasHighlightReels}\n";

            }

            using (FileStream fs = File.Create(@"c:\dev\PrivacyDataStore.csv"))
            {
                byte[] info = new UTF8Encoding(true).GetBytes(dataString);
                fs.Write(info, 0, info.Length);
            }

            foreach (InstaUserShort follower in followers.Value)
            {
                uniqueNames.Remove(follower.UserName);
                verified.Remove(follower.UserName);
            }

            Console.WriteLine("People that you follow but that don't follow you back:\n" +
                "------------------------\n" +
                "Public accounts:\n" +
                "------------------------");

            Console.ForegroundColor = ConsoleColor.Yellow;
            foreach (string vip in verified)
                Console.WriteLine(vip);

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("------------------------\n" +
                "Non-verified accounts:\n" +
                "------------------------");

            Console.ForegroundColor = ConsoleColor.Yellow;
            foreach (string name in uniqueNames)
                Console.WriteLine(name);

            Console.ForegroundColor = ConsoleColor.White;

        }

    }
}
